FROM node:lts as builder

WORKDIR /app

ADD package.json .

RUN yarn install

ADD . .

RUN yarn gridsome build

FROM bitnami/nginx

COPY --from=builder /app/dist /app
