# Default starter for Gridsome

This is the project you get when you run `gridsome create new-project`.

### 1. Install Gridsome CLI tool if you don't have

`npm install --global @gridsome/cli`

### 2. Create a Gridsome project

1. `gridsome create my-gridsome-site` to install default starter
2. `cd my-gridsome-site` to open the folder
3. `gridsome develop` to start a local dev server at `http://localhost:8080`
4. Happy coding 🎉🙌

# Run project on Netlify

## how to connect git repository to netlify
https://medium.com/captain-data-blog/how-to-deploy-any-git-repository-to-netlify-1a553b8e8e39

## Site URL

* Google (main): https://goofy.devops.iteratec.dev/

## manage Content in CMS
https://app.graphcms.com/9f3c35a0ee1a4697af809598f08db6d8/master

We use GraphCMS
