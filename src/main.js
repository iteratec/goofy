// This is the main.js file. Import global CSS and scripts here.
// The Client API can be used here. Learn more: gridsome.org/docs/client-api

import DefaultLayout from '~/layouts/Default.vue'
import Buefy from 'buefy'
import VueLazyload from 'vue-lazyload'
import Vuex from 'vuex'
import {getFirebase} from './firebase';


import '~/assets/styles.scss'

export default function (Vue, { head, router, isClient, appOptions }) {
  head.link.push({
    rel: 'stylesheet',
    href: 'https://use.fontawesome.com/releases/v5.2.0/css/all.css'
  })

  head.link.push({
    rel: 'stylesheet',
    href: 'https://cdn.snipcart.com/themes/v3.0.13/default/snipcart.css'
  })

  head.script.push({
    type: 'text/javascript',
    src: 'https://cdn.snipcart.com/themes/v3.0.13/default/snipcart.js',
    body: true,
  })

  head.link.push({
    rel: 'stylesheet',
    href: 'https://www.gstatic.com/firebasejs/ui/4.5.0/firebase-ui-auth.css'
  })

  head.script.push({
    type: 'text/javascript',
    src: 'https://www.gstatic.com/firebasejs/ui/4.5.0/firebase-ui-auth.js',
    //body: true,
  })

  Vue.use(VueLazyload)

  Vue.use(Buefy, {
    defaultIconPack: 'fas' // Font Awesome Solid
  })

  // Set default layout as a global component
  Vue.component('Layout', DefaultLayout)
  // Add attributes to HTML tag
  head.htmlAttrs = {lang: 'de'}
  // Add attributes to BODY tag

  Vue.use(Vuex)

  Vue.config.errorHandler = (error, vm) => {
    const code = error.response ? error.response.status : 500
    const message = error.response && error.response.data ? error.response.data.message : error.request
    const componentName = vm.$options.name

    console.error(`Error ${code} [${componentName}] : ${message || error}`)
  }

  appOptions.store = new Vuex.Store({
    state: {
      station: null,
      inventory: new Map(),
      openProduct: null,
      currentUser: null,
      invalidUser: false
    },
    mutations: {
      setStation(state, station) {
        console.log("Station set to " + JSON.stringify(station))
        state.station = station
      },
      setInventory(state, inventory) {
        state.inventory = inventory;
      },
      setOpenProduct(state, openProduct) {
        state.openProduct = openProduct;
      },
      UPDATE_INVENTORY(state, inventory) {
        state.inventory.set(inventory[0], inventory[1])
      },
      INVENTORY_UPDATED(state) {
      },
      setCurrentUser(state, currentUser) {
        state.currentUser = currentUser;
        state.invalidUser = false
      },
      logout(state) {
        state.currentUser = null
        state.invalidUser = false
      },
      setInvalidUser(state, flag) {
        console.log("setInvalidUser(" + flag + ")")
        state.invalidUser = flag;
        if (flag) {
          state.currentUser = null;
        }
      }
    },
    getters: {
      getInventory: (state) => (stationId, productId) => {
        return state.inventory.get(stationId)[productId];
      },
      getInventoryForStation: (state) => (stationId) => {
        return state.inventory.get(stationId);
      },
      getInventoryForCurrentStation: (state) => {
        return state.inventory.get(state.station.id);
      },
      invalidUser: state => {
        console.log("getter invalidUser()");
        return state.invalidUser;
      }
    },
    actions: {
      logout({commit}){
        getFirebase().auth().signOut().then(() => {
          commit('logout')
        }).catch((error) => {
          console.error(error)
        });
      }
    }
  });
}
