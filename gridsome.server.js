// Server API makes it possible to hook into various parts of Gridsome
// on server-side and add custom data to the GraphQL data layer.
// Learn more: https://gridsome.org/docs/server-api/

// Changes here require a server restart.
// To restart press CTRL + C in terminal and run `gridsome develop`

module.exports = function (api) {
  const axios = require('axios')
  const fs = require('fs')
  api.createPages(async ({ graphql, createPage }) => {
    const { data } = await graphql(`
      {
        gcms {
          products {
            id
          }
          servicePages {
            name
          }
        }
      }
    `);

    data.gcms.products.forEach(node => {
      createPage({
        path: `/product/${node.id}`,
        component: './src/templates/Product.vue',
        context: {
          id: node.id,
        },
      });
    });

    data.gcms.servicePages.forEach(node => {
      createPage({
        path: `/service/${node.name}`,
        component: './src/templates/ServicePages.vue',
        context: {
          name: node.name,
        },
      });
    });
  });
}
