const functions = require('firebase-functions');
// require and initialize the admin SDK
const admin = require('firebase-admin');

const axios = require('axios');
const cors = require('cors')({origin: true});

const config = functions.config()
admin.initializeApp();
const db = admin.firestore();

const runtimeOpts = {
  timeoutSeconds: 15,
  memory: '128MB'
}

// https://europe-west2-foody-08266820.cloudfunctions.net/snipCartWebhook
// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
/*
* This ist the entry point for the Snipcart Webhook, see https://app.snipcart.com/dashboard/webhooks
*/
exports.snipCartWebhook = functions
  .runWith(runtimeOpts)
  .region('europe-west2')
  .https.onRequest((request, response) => {
    console.log(JSON.stringify(request.body))
    if (request.body.eventName === 'order.completed') {
      orderCompleted(request.body.content)
        .then(result => response.status(200).send("Inventory Updated"))
        .catch(err => {
          console.error(err)
          response.status(500).send('Server Error')
        })
    } else {
      response.status(200).send('event ignored')
    }
  });

async function orderCompleted(content) {
  console.log('Content:' + JSON.stringify(content))
  const customFields = JSON.parse(content.customFieldsJson)
  const stationOrderMap = content.items.reduce((stationOrderMap, item) => {
    console.log('ItemID:' + item.id)
    const itemIdParts = item.id.split("-")
    const productId = itemIdParts[0]
    const stationPlz = itemIdParts.length > 1 ? itemIdParts[1] : "unknown"
    const quantity = item.quantity
    console.log('Found order entry for stationPlz:', stationPlz, 'productId:', productId, 'quantity:', quantity)
    let stationOrders = stationOrderMap[stationPlz] ? stationOrderMap[stationPlz] : [];
    stationOrders.push({
      productId: productId,
      quantity: quantity
    })
    stationOrderMap.set(stationPlz, stationOrders)
    return stationOrderMap
  }, new Map())
  console.log("stationOrderMap:", stationOrderMap)
  return Promise.all(Array.from(stationOrderMap,([key, value]) => updateStationInventory(key, value)))
}

async function updateStationInventory(stationPlz, stationOrders) {
  console.log("Update Inventory for station", stationPlz, "with orders", JSON.stringify(stationOrders))
  return db.collection('stations').where('plz', '==', stationPlz).get()
  .then(snapshot => {
    console.log("found station", stationPlz, "in database")
    snapshot.forEach( stationDoc => {
      let stationId = stationDoc.id;
      let station = stationDoc.data()
      if (station.plz !== stationPlz) {
        console.log("do not change inventory for station", stationDoc.id, "with PLZ", station.plz)
      } else {
        console.log("adjust inventory for station", stationDoc.id, "with PLZ", stationPlz);
        let products = station.products || {};
        console.log("current inventory count for products", "is", products);
        stationOrders.forEach(orderEntry => {
          console.log('OrderEntry: ' +  JSON.stringify(orderEntry))
          if (!products[orderEntry.productId]) {
            console.log("did not found inventory count for product:", orderEntry.productId, "resetting it to 999");
            products[orderEntry.productId] = 999;
          }
          products[orderEntry.productId] -= orderEntry.quantity;
        })
        console.log("update inventory in database with new products map:", JSON.stringify(products));
        db.collection('stations').doc(stationId).update({'products':products})
            .then(msg => console.log('Success: ' + msg))
            .catch(err => console.error(err))
      }
    });
  })
  .catch(err => {
    console.log('Error getting documents', err);
  });
}

/*
* This endpoint is used by the client to get the current inventory
*/
exports.inventory = functions
  .runWith(runtimeOpts)
  .region('europe-west2')
  .https.onRequest((req, res) => {
  cors(req,res,() => {
    return axios.get('https://app.snipcart.com/api/products',{
      auth: {
        username: config.snipcart.apikey,
        password: ''
      }}).then(response => {
      console.log(response.data);
      return res.status(200).json(
        response.data.items.map(it => {
          return {
            id: it.userDefinedId,
            name: it.name,
            stock: it.stock,
            totalStock: it.totalStock
          }
        })
      )
    }).catch(err => {
      console.log(err)
      return res.status(500).json({error: err})
    })
  })
});

exports.updateSnipcartInventory = functions
  .runWith(runtimeOpts)
  .region('europe-west2')
  .firestore.document('stations/{station}')
  .onUpdate((change, context) => {
    const newStationInventory = change.after.data();
    const previousStationInventory = change.before.data();
    console.log("Inventory change: ", context, "previous station inventory: ", previousStationInventory, "new station inventory: ", newStationInventory);
    if (newStationInventory.products === undefined) {
      return 0;
    }
    return Promise.all(Object.entries(newStationInventory.products).map(([productId, count]) =>  {
      const snipcartProductId = productId + "-" + newStationInventory.plz
      console.log(`updateSnipcartInventory: axios.put("https://app.snipcart.com/api/products/${snipcartProductId}")`)
      return axios.put(`https://app.snipcart.com/api/products/${snipcartProductId}`,
        {
          'inventoryManagementMethod': 'Single',
          'stock': count,
          'allowOutOfStockPurchases': false
        },
        {
          auth: {
            username: config.snipcart.apikey,
            password: ''
          },
          Accept: "application/json",
          "Content-type": "application/json"
        }).then(response => {
          console.log("axios.put response .status", response.status);
        }, err => {
          console.log("axios.put error", err);
        }
      )
    }))
  })


