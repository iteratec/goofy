// This is where project configuration and plugin options are located.
// Learn more: https://gridsome.org/docs/config

// Changes here require a server restart.
// To restart press CTRL + C in terminal and run `gridsome develop`

module.exports = {
  siteName: 'Foody',
  titleTemplate: '%s - iteratec',
  chainWebpack: config => {
    config.resolve.alias.set('@images', '@/assets/images')
  },
  plugins: [
    {
      use: '@gridsome/source-graphql',
      options: {
        url: process.env.GRAPHCMS_ENDPOINT,
        fieldName: 'gcms',
        typeName: 'gcmsTypes',
        headers: {
          Authorization: `Bearer ${process.env.AUTH_TOKEN}`,
        },
      },
    },
    {
      use: 'gridsome-plugin-segment-js',
      options: {
        prodKey: 'QpveqZzmd9NnRsbop1ZUxXCmljjuAhpL',
        devKey: 'QpveqZzmd9NnRsbop1ZUxXCmljjuAhpL',
        trackPage: true, // Defaults to false - will automatically send page views,
        pageCategory: 'gridsome-page' // Optional category value
      },
    },
  ]
}

